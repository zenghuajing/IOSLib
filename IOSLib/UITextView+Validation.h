//
//  UITextView+Validation.h
//  IOSLib
//
//  Created by Kevin Zeng on 2017/4/13.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Verify and replace the illegal character input
 */
@interface UITextView (Validation)

@property (nonatomic, strong, readonly) NSArray<NSString *> *illegalStrings;//Default contains characters ' and " to avoid JSON parsing errors

- (void)addIllegalString:(NSString *)string;
- (void)removeIllegalString:(NSString *)string;

@end
