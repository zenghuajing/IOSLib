//
//  NSString+Size.m
//  IOSLib
//
//  Created by Kevin Zeng on 2017/3/22.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import "NSString+Size.h"

@implementation NSString (Size)

- (CGFloat)heightWithFont:(UIFont *)font withParagraphStyle:(NSParagraphStyle *)paragraphStyle withinSize:(CGSize)size {
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    if (font) {
        [attributes setObject:font forKey:NSFontAttributeName];
    }
    
    if (paragraphStyle) {
        [attributes setObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
    }
    
    CGSize textSize = [self boundingRectWithSize:size options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attributes context:nil].size;
    
    return textSize.height;
}

- (CGFloat)heightWithFontSize:(CGFloat)fontSize withinSize:(CGSize)size {
    return [self heightWithFont:[UIFont systemFontOfSize:fontSize] withParagraphStyle:nil withinSize:size];
}

- (CGFloat)heightWithBoldFontSize:(CGFloat)fontSize withinSize:(CGSize)size {
    return [self heightWithFont:[UIFont boldSystemFontOfSize:fontSize] withParagraphStyle:nil withinSize:size];
}

- (CGFloat)widthWithFont:(UIFont *)font withParagraphStyle:(NSParagraphStyle *)paragraphStyle {
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    if (font) {
        [attributes setObject:font forKey:NSFontAttributeName];
    }
    if (paragraphStyle) {
        [attributes setObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
    }
    
    CGSize textSize = [self boundingRectWithSize:CGSizeMake(MAXFLOAT, font.pointSize) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attributes context:nil].size;
    
    return textSize.width;
}

- (CGFloat)widthWithFontSize:(CGFloat)fontSize {
    return [self widthWithFont:[UIFont systemFontOfSize:fontSize] withParagraphStyle:nil];
}

- (CGFloat)widthWithBoldFontSize:(CGFloat)fontSize {
    return [self widthWithFont:[UIFont boldSystemFontOfSize:fontSize] withParagraphStyle:nil];
}

@end
