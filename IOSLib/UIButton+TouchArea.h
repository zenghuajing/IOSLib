//
//  UIButton+TouchArea.h
//  IOSLib
//
//  Created by Kevin Zeng on 2017/3/22.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Setting response area
 */
@interface UIButton (TouchArea)

/**
 Increase the response area with different size
 */
- (void)enlargeTouchAreaWithTop:(CGFloat)top left:(CGFloat)left bottom:(CGFloat)bottom right:(CGFloat)right;

/**
 Increase the response area with the same size
 */
- (void)enlargeTouchArea:(CGFloat)size;

@end
