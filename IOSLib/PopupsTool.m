//
//  PopupsTool.m
//  IOSLib
//
//  Created by Kevin Zeng on 2017/3/22.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import "PopupsTool.h"

#define kScreenWidth   ([UIScreen mainScreen].bounds.size.width)
#define kScreenHeight  ([UIScreen mainScreen].bounds.size.height)
#define kKeyWindow     ([UIApplication sharedApplication].keyWindow)

@interface PopupsTool ()<UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIView *maskView;
@property (nonatomic, strong) UIView *view;
@property (nonatomic, strong) UITapGestureRecognizer *tap;
@property (nonatomic, copy) void (^dismissCompletion)(void);
@property (nonatomic, strong) UIView *alertView;
@property (nonatomic, strong) UILabel *messageLabel;

@end

@implementation PopupsTool

static PopupsTool *_popupsTool;

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _popupsTool = [[PopupsTool alloc] init];
    });
    return _popupsTool;
}

- (void)popupView:(UIView *)view backgroundColor:(UIColor *)backgroundColor animationType:(PopupsToolAnimationType)animationType completion:(void (^)(void))completion {
    [self popupView:view backgroundColor:backgroundColor animationType:animationType completion:completion showCloseBtn:NO tapOutsideToDismiss:NO dismissCompletion:nil];
}

- (void)popupView:(UIView *)view backgroundColor:(UIColor *)backgroundColor animationType:(PopupsToolAnimationType)animationType completion:(void (^)(void))completion showCloseBtn:(BOOL)show tapOutsideToDismiss:(BOOL)tap dismissCompletion:(void (^)(void))dismissCompletion{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!weakSelf) return;
        __typeof (&*self) strongSelf = weakSelf;
        strongSelf.maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        strongSelf.maskView.backgroundColor = backgroundColor;
        strongSelf.view = view;
        [strongSelf.maskView addSubview:strongSelf.view];
        [kKeyWindow addSubview:strongSelf.maskView];
        if (show) {
            UIButton *closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(view.frame.size.width-24, 0, 24, 24)];
            [closeBtn setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
            [closeBtn addTarget:self action:@selector(_dismiss) forControlEvents:UIControlEventTouchUpInside];
            strongSelf.dismissCompletion = dismissCompletion;
            [view addSubview:closeBtn];
        }
        
        if (tap){
            strongSelf.tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_dismiss)];
            strongSelf.tap.delegate = self;
            strongSelf.dismissCompletion = dismissCompletion;
            [kKeyWindow addGestureRecognizer:strongSelf.tap];
        }
        
        switch (animationType) {
            case PopupsToolAnimationNone:
            {
                if (completion) {
                    completion();
                }
                break;
            }
                
            case PopupsToolAnimationFade:
            {
                strongSelf.maskView.alpha = 0;
                __weak typeof(self) weakSelf = self;
                [UIView animateWithDuration:0.3 animations:^{
                    if (!weakSelf) return;
                    __typeof (&*self) strongSelf = weakSelf;
                    strongSelf.maskView.alpha = 1;
                } completion:^(BOOL finished) {
                    if (completion) {
                        completion();
                    }
                }];
                break;
            }
                
            case PopupsToolAnimationScale:
            {
                strongSelf.maskView.transform = CGAffineTransformScale(CGAffineTransformIdentity, CGFLOAT_MIN, CGFLOAT_MIN);
                __weak typeof(self) weakSelf = self;
                [UIView animateWithDuration:0.3 animations:^{
                    if (!weakSelf) return;
                    __typeof (&*self) strongSelf = weakSelf;
                    strongSelf.maskView.transform = CGAffineTransformIdentity;;
                } completion:^(BOOL finished) {
                    if (completion) {
                        completion();
                    }
                }];
                break;
            }
                
            case PopupsToolAnimationScaleAndFade:
            {
                strongSelf.maskView.transform = CGAffineTransformScale(CGAffineTransformIdentity, CGFLOAT_MIN, CGFLOAT_MIN);
                strongSelf.maskView.alpha = 0;
                __weak typeof(self) weakSelf = self;
                [UIView animateWithDuration:0.3 animations:^{
                    if (!weakSelf) return;
                    __typeof (&*self) strongSelf = weakSelf;
                    strongSelf.maskView.transform = CGAffineTransformIdentity;;
                    strongSelf.maskView.alpha = 1;
                } completion:^(BOOL finished) {
                    if (completion) {
                        completion();
                    }
                }];
                break;
            }
                
            case PopupsToolAnimationTranslateFromBottom:
            {
                strongSelf.maskView.frame = CGRectMake(0, kScreenHeight, kScreenWidth, kScreenHeight);
                __weak typeof(self) weakSelf = self;
                [UIView animateWithDuration:0.5 animations:^{
                    if (!weakSelf) return;
                    __typeof (&*self) strongSelf = weakSelf;
                    strongSelf.maskView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
                } completion:^(BOOL finished) {
                    if (completion) {
                        completion();
                    }
                }];
                break;
            }
                
            case PopupsToolAnimationTranslateToBottom:
            {
                if (completion) {
                    completion();
                }
                break;
            }
        }
    });
}

- (void)dismissView:(PopupsToolAnimationType)animationType completion:(void (^)(void))completion{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!weakSelf) return;
        __typeof (&*self) strongSelf = weakSelf;
        switch (animationType) {
            case PopupsToolAnimationNone:
            {
                [strongSelf.maskView removeFromSuperview];
                if (completion) {
                    completion();
                }
                break;
            }
                
            case PopupsToolAnimationFade:
            {
                __weak typeof(self) weakSelf = self;
                [UIView animateWithDuration:0.3 animations:^{
                    if (!weakSelf) return;
                    __typeof (&*self) strongSelf = weakSelf;
                    strongSelf.maskView.alpha = 0;
                } completion:^(BOOL finished) {
                    if (!weakSelf) return;
                    __typeof (&*self) strongSelf = weakSelf;
                    [strongSelf.maskView removeFromSuperview];
                    if (completion) {
                        completion();
                    }
                }];
                break;
            }
                
            case PopupsToolAnimationScale:
            {
                __weak typeof(self) weakSelf = self;
                [UIView animateWithDuration:0.3 animations:^{
                    if (!weakSelf) return;
                    __typeof (&*self) strongSelf = weakSelf;
                    strongSelf.maskView.transform = CGAffineTransformScale(CGAffineTransformIdentity, CGFLOAT_MIN, CGFLOAT_MIN);
                } completion:^(BOOL finished) {
                    if (!weakSelf) return;
                    __typeof (&*self) strongSelf = weakSelf;
                    [strongSelf.maskView removeFromSuperview];
                    if (completion) {
                        completion();
                    }
                }];
                break;
            }
                
            case PopupsToolAnimationScaleAndFade:
            {
                __weak typeof(self) weakSelf = self;
                [UIView animateWithDuration:0.3 animations:^{
                    if (!weakSelf) return;
                    __typeof (&*self) strongSelf = weakSelf;
                    strongSelf.maskView.transform = CGAffineTransformMakeScale(CGFLOAT_MIN, CGFLOAT_MIN);
                    strongSelf.maskView.alpha = 0;
                } completion:^(BOOL finished) {
                    if (!weakSelf) return;
                    __typeof (&*self) strongSelf = weakSelf;
                    [strongSelf.maskView removeFromSuperview];
                    if (completion) {
                        completion();
                    }
                }];
                break;
            }
                
            case PopupsToolAnimationTranslateToBottom:
            {
                __weak typeof(self) weakSelf = self;
                [UIView animateWithDuration:0.5 animations:^{
                    if (!weakSelf) return;
                    __typeof (&*self) strongSelf = weakSelf;
                    strongSelf.maskView.frame = CGRectMake(0, kScreenHeight, kScreenWidth, kScreenHeight);
                } completion:^(BOOL finished) {
                    if (completion) {
                        completion();
                    }
                }];
                break;
            }
                
            case PopupsToolAnimationTranslateFromBottom:
            {
                if (completion) {
                    completion();
                }
                break;
            }
        }
        
        if (strongSelf.tap) {
            [kKeyWindow removeGestureRecognizer:strongSelf.tap];
            strongSelf.tap = nil;
        }
    });
}

- (void)showMessage:(NSString *)text completion:(void (^)(void))completion {
    [kKeyWindow endEditing:YES];
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!weakSelf) return;
        __typeof (&*self) strongSelf = weakSelf;
        strongSelf.messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth/2, kScreenHeight)];
        strongSelf.messageLabel.numberOfLines = 0;
        strongSelf.messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        strongSelf.messageLabel.textAlignment = NSTextAlignmentCenter;
        strongSelf.messageLabel.textColor = [UIColor whiteColor];
        strongSelf.messageLabel.font = [UIFont boldSystemFontOfSize:15];
        strongSelf.messageLabel.text = text;
        [strongSelf.messageLabel sizeToFit];
        
        strongSelf.alertView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, strongSelf.messageLabel.frame.size.width+40, strongSelf.messageLabel.frame.size.height+40)];
        strongSelf.alertView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0];
        strongSelf.alertView.layer.cornerRadius = 5;
        strongSelf.alertView.layer.masksToBounds = YES;
        strongSelf.alertView.center = CGPointMake(kScreenWidth/2, kScreenHeight/2);
        strongSelf.messageLabel.center = CGPointMake(strongSelf.alertView.frame.size.width/2, strongSelf.alertView.frame.size.height/2);
        [strongSelf.alertView addSubview:strongSelf.messageLabel];
        [kKeyWindow addSubview:strongSelf.alertView];
        
        [UIView animateWithDuration:1.0 delay:1.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            if (!weakSelf) return;
            __typeof (&*self) strongSelf = weakSelf;
            strongSelf.alertView.alpha = 0.0;
        } completion:^(BOOL finished) {
            if (!weakSelf) return;
            __typeof (&*self) strongSelf = weakSelf;
            [strongSelf.alertView removeFromSuperview];
            if (completion) {
                completion();
            }
        }];
    });
}

#pragma mark - Action
- (void)_dismiss {
    [self dismissView:PopupsToolAnimationFade completion:_dismissCompletion];
}

#pragma mark - <UIGestureRecognizerDelegate>
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (touch.view != _maskView) {
        return NO;
    }
    return YES;
}

@end
