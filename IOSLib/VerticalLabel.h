//
//  VerticalLabel.h
//  IOSLib
//
//  Created by Kevin Zeng on 2017/3/22.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Text writing direction
 */
typedef NS_ENUM(NSInteger, VerticalLabelWritingDirection)
{
    VerticalLabelWritingDirectionLeftToRight,// Default
    VerticalLabelWritingDirectionRightToLeft,
};

@interface VerticalLabel : UILabel

@property (nonatomic, assign) VerticalLabelWritingDirection writingDirection;
@property (nonatomic, assign) CGFloat space;

- (instancetype)initWithFrame:(CGRect)frame text:(NSString *)text direction:(VerticalLabelWritingDirection)direction space:(CGFloat)space font:(UIFont *)font textColor:(UIColor *)textColor NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_EXTENSION_UNAVAILABLE("Use -initWithFrame:withText:withTextDirection: withSpace:withFont:withTextColor:");
- (instancetype)initWithFrame:(CGRect)frame NS_EXTENSION_UNAVAILABLE("Use -initWithFrame:withText:withTextDirection: withSpace:withFont:withTextColor:");

- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_DESIGNATED_INITIALIZER;
- (void)encodeWithCoder:(NSCoder *)aCoder;

@end

/**
 Calculate the size required for VerticalLabel‘s text to display
 */
@interface NSString (VerticalLabel)

- (CGSize)verticalLabelWithinSize:(CGSize)size font:(UIFont *)font space:(CGFloat)space;

@end
