//
//  main.m
//  IOSLib
//
//  Created by Kevin Zeng on 2017/4/13.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
