//
//  UINavigationBar+Style.m
//  IOSLib
//
//  Created by Kevin Zeng on 2017/3/23.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import "UINavigationBar+Style.h"

@implementation UINavigationBar (Style)

- (void)setBackgroundColor:(UIColor *)backgroundColor {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, -20, [UIScreen mainScreen].bounds.size.width, 64)];
    view.backgroundColor = backgroundColor;
    [self setValue:view forKey:@"backgroundView"];
}

@end
