//
//  NSObject+Unicode.m
//  IOSLib
//
//  Created by Kevin Zeng on 2017/3/23.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import "NSObject+Unicode.h"

@implementation NSObject (Unicode)

+ (NSString *)stringByReplaceUnicode:(NSString *)string {
    NSMutableString *convertedString = [string mutableCopy];
    [convertedString replaceOccurrencesOfString:@"\\U" withString:@"\\u" options:0 range:NSMakeRange(0, convertedString.length)];
    CFStringRef transform = CFSTR("Any-Hex/Java");
    CFStringTransform((__bridge CFMutableStringRef)convertedString, NULL, transform, YES);
    return convertedString;
}

@end
