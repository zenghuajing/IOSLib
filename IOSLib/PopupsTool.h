//
//  PopupsTool.h
//  IOSLib
//
//  Created by Kevin Zeng on 2017/3/22.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import <UIKit/UIKit.h>

#define POPUPSTOOL_SINGLETON ([PopupsTool sharedInstance])

/**
 Popup animation effects
 */
typedef NS_ENUM(NSInteger, PopupsToolAnimationType){
    PopupsToolAnimationNone,// No animation（Default）
    PopupsToolAnimationFade,// Transparency gradient
    PopupsToolAnimationScale,// Zoom
    PopupsToolAnimationScaleAndFade,// Zoom and transparency gradient
    PopupsToolAnimationTranslateFromBottom,// Move from bottom of screen
    PopupsToolAnimationTranslateToBottom,// Remove from bottom of screen
};

/**
 Popup view tool
 */
@interface PopupsTool : NSObject

/**
 Get singleton
 */
+ (instancetype)sharedInstance;

/**
 Popup
 */
- (void)popupView:(UIView *)view backgroundColor:(UIColor *)backgroundColor animationType:(PopupsToolAnimationType)animationType completion:(void(^)(void))completion;
- (void)popupView:(UIView *)view backgroundColor:(UIColor *)backgroundColor animationType:(PopupsToolAnimationType)animationType completion:(void(^)(void))completion showCloseBtn:(BOOL)show tapOutsideToDismiss:(BOOL)tap dismissCompletion:(void(^)(void))dismissCompletion;

/**
 Dismiss
 */
- (void)dismissView:(PopupsToolAnimationType)animationType completion:(void(^)(void))completion;

/**
 Message
 */
- (void)showMessage:(NSString *)text completion:(void(^)(void))completion;

@end
