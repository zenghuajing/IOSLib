//
//  NSString+Size.h
//  IOSLib
//
//  Created by Kevin Zeng on 2017/3/22.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The size of the string to be displayed
 */
@interface NSString (Size)

/**
 Return the height required to display a string
 */
- (CGFloat)heightWithFont:(UIFont *)font withParagraphStyle:(NSParagraphStyle *)paragraphStyle withinSize:(CGSize)size;
- (CGFloat)heightWithFontSize:(CGFloat)fontSize withinSize:(CGSize)size;
- (CGFloat)heightWithBoldFontSize:(CGFloat)fontSize withinSize:(CGSize)size;

/**
 Return the width of a single line display string
 */
- (CGFloat)widthWithFont:(UIFont *)font withParagraphStyle:(NSParagraphStyle *)paragraphStyle;
- (CGFloat)widthWithFontSize:(CGFloat)fontSize;
- (CGFloat)widthWithBoldFontSize:(CGFloat)fontSize;

@end
