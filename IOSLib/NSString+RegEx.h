//
//  NSString+RegEx.h
//  IOSLib
//
//  Created by Kevin Zeng on 2017/3/22.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 Regular expression validation
 */
@interface NSString (RegEx)

/** 
 Return the array of results that match the regular expression
 */
- (NSArray<NSTextCheckingResult *> *)searchByRegEx:(NSString *)regex;

/**
 Verify whether matching regular expressions
 */
- (BOOL)matchRegEx:(NSString *)regex;

/**
 Convenient verification method
 */
- (BOOL)isTelephone;// CMCC, CUCC, CTC, PHS, etc.
- (BOOL)isPassword;// 6-20 characters(Letters and numbers)
- (BOOL)isNumber;
- (BOOL)isEmail;
- (BOOL)isUrl;

@end
