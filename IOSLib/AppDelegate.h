//
//  AppDelegate.h
//  IOSLib
//
//  Created by Kevin Zeng on 2017/4/13.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

