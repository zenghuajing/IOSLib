//
//  NSSet+Unicode.m
//  IOSLib
//
//  Created by Kevin Zeng on 2017/3/23.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import <objc/runtime.h>
#import "NSSet+Unicode.h"
#import "NSObject+Unicode.h"

@implementation NSSet (Unicode)

+ (void)load {
    method_exchangeImplementations(class_getInstanceMethod([self class], @selector(description)), class_getInstanceMethod([self class], @selector(_description)));
    method_exchangeImplementations(class_getInstanceMethod([self class], @selector(descriptionWithLocale:)), class_getInstanceMethod([self class], @selector(_descriptionWithLocale:)));
    method_exchangeImplementations(class_getInstanceMethod([self class], @selector(descriptionWithLocale:indent:)), class_getInstanceMethod([self class], @selector(_descriptionWithLocale:indent:)));
}

- (NSString *)_description {
    return [NSObject stringByReplaceUnicode:[self _description]];
}

- (NSString *)_descriptionWithLocale:(nullable id)locale {
    return [NSObject stringByReplaceUnicode:[self _descriptionWithLocale:locale]];
}

- (NSString *)_descriptionWithLocale:(nullable id)locale indent:(NSUInteger)level {
    return [NSObject stringByReplaceUnicode:[self _descriptionWithLocale:locale indent:level]];
}

@end
