//
//  NSObject+Unicode.h
//  IOSLib
//
//  Created by Kevin Zeng on 2017/3/23.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Unicode)

/**
 Replace unicode character with UTF-8 string
 */
+ (NSString *)stringByReplaceUnicode:(NSString *)string;

@end
