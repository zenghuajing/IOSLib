//
//  NSString+RegEx.m
//  IOSLib
//
//  Created by Kevin Zeng on 2017/3/22.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import "NSString+RegEx.h"

@implementation NSString (RegEx)

- (NSArray<NSTextCheckingResult *> *)searchByRegEx:(NSString *)regex {
    NSRegularExpression *regularExpression = [NSRegularExpression regularExpressionWithPattern:regex options:NSRegularExpressionCaseInsensitive error:nil];
    
    NSArray *matches = [regularExpression matchesInString:self options:NSMatchingReportProgress range:NSMakeRange(0, self.length)];
    
    return matches;
}

- (BOOL)matchRegEx:(NSString *)regex {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [predicate evaluateWithObject:self];
}

- (BOOL)isTelephone {
    NSString *MP = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";// Mobile Phone（手机）
    NSString *CMCC = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";// China Mobile Communications Corporation（中国移动）
    NSString *CUCC = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";// China Unicom Communications Corporation（中国联通）
    NSString *CTC = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";// China Telecommunications Corporation（中国电信）
    NSString *PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";// Personal Handphone System（小灵通）
    
    return [self matchRegEx:MP] || [self matchRegEx:CMCC] || [self matchRegEx:CUCC] || [self matchRegEx:CTC] || [self matchRegEx:PHS];
}

- (BOOL)isPassword {
    NSString *regex = @"(^[A-Za-z0-9]{6,20}$)";
    return [self matchRegEx:regex];
}

- (BOOL)isNumber {
    NSString *regex = @"^-?\\d+.?\\d?";
    return [self matchRegEx:regex];
}

- (BOOL)isEmail {
    NSString *regex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    return [self matchRegEx:regex];
}

- (BOOL)isUrl {
    NSString *regex = @"http(s)?:\\/\\/([\\w-]+\\.)+[\\w-]+(\\/[\\w- .\\/?%&=]*)?";
    return [self matchRegEx:regex];
}

@end
