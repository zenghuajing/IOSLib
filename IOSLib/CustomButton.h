//
//  CustomButton.h
//  IOSLib
//
//  Created by Kevin Zeng on 2017/4/6.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import <UIKit/UIKit.h>

// Button image orientation
typedef NS_ENUM(NSInteger, CustomButtonImageDirection)
{
    CustomButtonImageDirectionLeft,// Default
    CustomButtonImageDirectionRight,
    CustomButtonImageDirectionTop,
    CustomButtonImageDirectionBottom,
};

@interface CustomButton : UIButton

@property (nonatomic, assign) CustomButtonImageDirection imageDirection;
@property (nonatomic, assign) CGFloat space;
@property (nonatomic, assign) CGSize titleSize;
@property (nonatomic, assign) CGSize imageSize;

- (instancetype)initWithFrame:(CGRect)frame imageDirection:(CustomButtonImageDirection)direction space:(CGFloat)space titleSize:(CGSize)titleSize iamgeSize:(CGSize)imageSize NS_DESIGNATED_INITIALIZER;
- (instancetype)initWithFrame:(CGRect)frame imageDirection:(CustomButtonImageDirection)direction space:(CGFloat)space;
- (instancetype)initWithFrame:(CGRect)frame imageDirection:(CustomButtonImageDirection)direction;
- (instancetype)initWithFrame:(CGRect)frame space:(CGFloat)space;

- (instancetype)init NS_EXTENSION_UNAVAILABLE("Use -initWithFrame:imageDirection:space:");
- (instancetype)initWithFrame:(CGRect)frame NS_EXTENSION_UNAVAILABLE("Use -initWithFrame:imageDirection:space:");

- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_DESIGNATED_INITIALIZER;
- (void)encodeWithCoder:(NSCoder *)aCoder;

@end
