//
//  UITextField+Validation.m
//  IOSLib
//
//  Created by Kevin Zeng on 2017/4/12.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import <objc/runtime.h>
#import "UITextField+Validation.h"

static inline void methodSwizzling(Class theClass, SEL originalSelector, SEL swizzledSelector) {
    Method methodA = class_getInstanceMethod(theClass, originalSelector);
    Method methodB = class_getInstanceMethod(theClass, swizzledSelector);
    
    BOOL isAdd = class_addMethod(theClass, originalSelector, method_getImplementation(methodB), method_getTypeEncoding(methodB));
    
    if (isAdd) {
        class_replaceMethod(theClass, swizzledSelector, method_getImplementation(methodA), method_getTypeEncoding(methodA));
    }else{
        method_exchangeImplementations(methodA, methodB);
    }
}

static const char *_illegalStrings = "_illegalStrings";

@interface UITextField ()
@property (nonatomic, strong, readwrite) NSArray<NSString *> *illegalStrings;
@end

@implementation UITextField (Validation)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        methodSwizzling(self, @selector(initWithFrame:), @selector(_initWithFrame:));
        methodSwizzling(self, @selector(initWithCoder:), @selector(_initWithCoder:));
    });
}

- (void)dealloc {
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)_initWithFrame:(CGRect)frame {
    [self config];
    return [self _initWithFrame:frame];
}

- (instancetype)_initWithCoder:(NSCoder *)aDecoder {
    [self config];
    return [self _initWithCoder:aDecoder];
}

- (void)config {
    [self addTarget:self action:@selector(valueChanged) forControlEvents:UIControlEventEditingChanged];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(valueChanged) name:UITextFieldTextDidChangeNotification object:nil];
    NSArray *illegalStrings = @[@"\"", @"\'"];
    [self setIllegalStrings:illegalStrings];
}

- (void)setIllegalStrings:(NSArray<NSString *> *)illegalStrings {
    objc_setAssociatedObject(self, _illegalStrings, illegalStrings, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSArray<NSString *> *)illegalStrings {
    return objc_getAssociatedObject(self, _illegalStrings);
}

- (void)addIllegalString:(NSString *)string {
    NSMutableArray *illegalStrings = [self.illegalStrings mutableCopy];
    if (![illegalStrings containsObject:string]) {
        [illegalStrings addObject:string];
        [self setIllegalStrings:illegalStrings];
    }
}

- (void)removeIllegalString:(NSString *)string {
    NSMutableArray *illegalStrings = [self.illegalStrings mutableCopy];
    if ([illegalStrings containsObject:string]) {
        [illegalStrings removeObject:string];
        [self setIllegalStrings:illegalStrings];
    }
}

#pragma mark - Action
- (void)valueChanged {
    NSString *text = self.text;
    NSArray *illegalStrings = self.illegalStrings;
    
    for (NSString *illegalString in illegalStrings) {
        if ([text containsString:illegalString]) {
            NSLog(@"非法字符：%@", illegalString);
            self.text = [text stringByReplacingOccurrencesOfString:illegalString withString:@""];
        }
    }
    
    NSLog(@"%@", self.text);
}

@end
