//
//  UIButton+TimeInterval.m
//  IOSLib
//
//  Created by Kevin Zeng on 2017/3/22.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import <objc/runtime.h>
#import "UIButton+TimeInterval.h"

static const NSTimeInterval _defaultInterval = 1;// Default is 1 second
static const char *_eventTimeInterval = "_eventTimeInterval";
static const char *_eventIsIgnoreEvent = "_eventIsIgnoreEvent";

@interface UIButton ()

@property (nonatomic, assign) BOOL isIgnoreEvent;

@end

@implementation UIButton (TimeInterval)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        SEL selectorA = @selector(sendAction:to:forEvent:);
        SEL selectorB = @selector(_sendAction:to:forEvent:);
        Method methodA = class_getInstanceMethod(self, selectorA);
        Method methodB = class_getInstanceMethod(self, selectorB);
        
        BOOL isAdd = class_addMethod(self, selectorA, method_getImplementation(methodB), method_getTypeEncoding(methodB));
        
        if (isAdd) {
            class_replaceMethod(self, selectorB, method_getImplementation(methodA), method_getTypeEncoding(methodA));
        }else{
            method_exchangeImplementations(methodA, methodB);
        }
    });
}

- (void)_sendAction:(SEL)action to:(id)target forEvent:(UIEvent *)event {
    self.eventTimeInterval = self.eventTimeInterval == 0 ? _defaultInterval : self.eventTimeInterval;
    if (self.isIgnoreEvent){
        return;
    }else if (self.eventTimeInterval > 0){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(self.eventTimeInterval * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self setIsIgnoreEvent:NO];
        });
    }
    
    self.isIgnoreEvent = YES;
    [self _sendAction:action to:target forEvent:event];
}

- (void)setIsIgnoreEvent:(BOOL)isIgnoreEvent {
    objc_setAssociatedObject(self, _eventIsIgnoreEvent, @(isIgnoreEvent), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)isIgnoreEvent {
    return [objc_getAssociatedObject(self, _eventIsIgnoreEvent) boolValue];
}

- (void)setEventTimeInterval:(NSTimeInterval)eventTimeInterval {
    objc_setAssociatedObject(self, _eventTimeInterval, @(eventTimeInterval), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSTimeInterval)eventTimeInterval {
    return [objc_getAssociatedObject(self, _eventTimeInterval) doubleValue];
}

@end
