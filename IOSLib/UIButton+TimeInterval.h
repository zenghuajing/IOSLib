//
//  UIButton+TimeInterval.h
//  IOSLib
//
//  Created by Kevin Zeng on 2017/3/22.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Response time interval
 */
@interface UIButton (TimeInterval)

@property (nonatomic, assign) NSTimeInterval eventTimeInterval;

@end
