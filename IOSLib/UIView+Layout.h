//
//  UIView+Layout.h
//  IOSLib
//
//  Created by Kevin Zeng on 2017/3/22.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Quick access to view location and size
 */
@interface UIView (Layout)

@property (nonatomic, assign) CGFloat top;
@property (nonatomic, assign) CGFloat left;
@property (nonatomic, assign) CGFloat bottom;
@property (nonatomic, assign) CGFloat right;

@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;

@end
