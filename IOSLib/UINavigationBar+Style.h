//
//  UINavigationBar+Style.h
//  IOSLib
//
//  Created by Kevin Zeng on 2017/3/23.
//  Copyright © 2017年 Kevin Zeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (Style)

/**
 Navigation bar and status bar color
 */
- (void)setBackgroundColor:(UIColor *)backgroundColor;

@end
